import React from "react";
import { ToDoInput } from "./Components/ToDoInput/ToDoInput";
import { ToDoList } from "./Components/ToDoList/ToDoList";
import { ToDoTitle } from "./Components/ToDoTitle/ToDoTitle";

class App extends React.Component {
  
  constructor (props) {
    super(props);
    this.state = { todos: [ 
      {id: Date.now(), done: false, name: 'Task1', description: 'Description1'},
      {id: Date.now()+1, done: false, name: 'Task2', description: 'Description2'},
      {id: Date.now()+2, done: false, name: 'Task3', description: 'Description3'},
      {id: Date.now()+3, done: false, name: 'Task4', description: 'Description4'},
      {id: Date.now()+4, done: false, name: 'Task5', description: 'Description5'}
      ]};
  }
  
  createToDoObject = (name, description) => {
    const todo = {
      id: Date.now(),
      done: false
    }

    this.setState ({
      todos: [{ ...todo, name, description }, ...this.state.todos]
    })
  }

  completeToDo = (id) => {
    const todoIndex = this.state.todos.findIndex(item => item.id === id);
    const todo = this.state.todos;

    todo[todoIndex].done = !todo[todoIndex].done;

    this.setState({
      todos: [...this.state.todos]
    });
  }

  deleteToDo = (id) => {
    const newTodos = this.state.todos.filter(item => item.id !== id);

    this.setState ({
      todos: newTodos
    });
  }

  render () {
    return (
      <div className="app">
        <ToDoTitle />
        <ToDoInput create={this.createToDoObject}/>
        <ToDoList complete={this.completeToDo} todos={this.state.todos} delete={this.deleteToDo}/>
      </div>
    );
  };
}

export default App;

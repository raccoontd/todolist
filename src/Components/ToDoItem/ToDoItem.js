import React from "react";
import './ToDoItem.css'

export class ToDoItem extends React.Component {
    render () {
        return (
            <li className='container' id={this.props.todo.id}>
                <input className='checkbox' type='checkbox' onChange={() => this.props.complete(this.props.todo.id)}/>
                <span className={this.props.todo.done ? 'done name' : 'name'}>
                        {this.props.todo.name}
                </span>
                <span className={this.props.todo.done ? 'done description' : 'description'}>
                        {this.props.todo.description}
                </span>
                <button className='button' onClick={() => this.props.delete(this.props.todo.id)}> &times; </button>
            </li>
        )
    };
}
import React from "react";
import { ToDoItem } from "../ToDoItem/ToDoItem";
import './ToDoList.css'

export class ToDoList extends React.Component {
    render () {
        const todoItems = this.props.todos.map(item => {
            return <ToDoItem 
                key={item.id} 
                todo={item}
                complete={this.props.complete}
                delete={this.props.delete}
            />
        })

        return (
            <ul className='list'>
                {todoItems}
            </ul>
        )
    };
}
import React from "react";
import './ToDoInput.css';

export class ToDoInput extends React.Component {
    constructor (props) {
        super (props);
        this.state = { name: '', description: '' }
    }

    handleChangeName = (event) => {
        this.setState({ name: event.target.value })
    }
    
    handleChangeDescription = (event) => {
        this.setState({ description: event.target.value })
    }

    onClick = () => {
        if (this.state.name === '') {
            return;
        }
        if (this.state.description === '') {
            return;
        }

        this.props.create(this.state.name, this.state.description);
        this.setState({name: ''})
        this.setState({description: ''})
    }

    render() {
        return (
            <div className='input'>
                <input 
                    placeholder='Name of the task'
                    value={this.state.name}
                    onChange={this.handleChangeName} 
                    type="text" 
                    className="inputName" 
                />
                <textarea
                    placeholder='What needs to be done'
                    value={this.state.description}
                    onChange={this.handleChangeDescription} 
                    type="text" 
                    className="inputDescription" 
                />
                <button className="button" onClick={this.onClick}>Add</button>
            </div>
        );
    }
}
import React from "react";
import './ToDoTitle.css'

export class ToDoTitle extends React.Component {
    render() {
        return (
            <h1 className='title'>ToDoList</h1>
        )
    };
}